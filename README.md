# AndroidRepo - Bot

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/025bfe51e380490695e8c0dd3c36a450)](https://app.codacy.com/gh/AmanoTeam/AndroidRepo?utm_source=github.com&utm_medium=referral&utm_content=AmanoTeam/AndroidRepo&utm_campaign=Badge_Grade_Settings)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![GitHub contributors](https://img.shields.io/github/contributors/AmanoTeam/AndroidRepo.svg)](https://GitHub.com/AmanoTeam/AndroidRepo/graphs/contributors/)

AndroidRepo-Bot is a bot made for [@AndroidRepo](https://t.me/AndroidRepo) (Telegram channel), it was initially thought only to update the Magisk modules in the channel, but we will improve it over time.

Developed in Python using the MTProto library [Pyrogram](https://github.com/pyrogram/pyrogram).
